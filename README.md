This project was originally create during CornHacks 2022. Please view our project's DevPost here:https://devpost.com/software/matt-s-retirement-plan

Project Description: Never lose an ebay auction again! This bot will snipe your desired auction for a desired bid price.

Project Contributors: Trenton Chramosta, Matthew Rokusek, John Reagan

Dependencies:
Java 1.8
Windows OS

Future TODO:
builder from .jar
cross-compatibility with Linux, macOS, and Android
