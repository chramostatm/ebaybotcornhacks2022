import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.File;

/**
 *  The AppWindow class displays the BorderPane for the eBay Auction Sniper app.
 *  @author John Reagan
 *  @author Matthew Rokusek
 *  @author Trenton Chramosta
 *  @version 2.0
 *  @since 2022-01-23
 */
public class AppWindow extends Application
{
    TimerSchedule timerSchedule = new TimerSchedule();

    @Override
    public void start(Stage stage)
    {
        // Add the image, welcome message, and button to a pane.
        BorderPane pane = new BorderPane();

        VBox top = new VBox();
        top.setPadding(new Insets(10, 10, 0, 10));
        MenuBar menuBar = new MenuBar();
        Menu menuFile = new Menu("FILE");
        MenuItem login = new MenuItem("Login");
        login.setOnAction(event ->
        {
            timerSchedule.login();
        });
        MenuItem exit = new MenuItem("Exit");
        exit.setOnAction(event ->
        {
            System.exit(0);
        });
        menuFile.getItems().addAll(login, new SeparatorMenuItem(), exit);
        Menu menuHelp = new Menu("HELP");
        MenuItem about = new MenuItem("About");
        about.setOnAction(event ->
        {
            showInfoDialogue("About eBay Auction Sniper",
                    "Version:\t1.0\nSince:\t2022/01/23\nAuthors:\t " +
                            "Trenton Chramosta\n\t\tJohn Reagan\n\t\tMatthew Rokusek" +
                            "\nThe eBay Auction Sniper was developed for the CornHacks" +
                            " 2022 conference at UNL. It allows the user to set a " +
                            "bot who will snipe any eBay auction ('Buy It Now' is not "+
                            "an auction). The only requirements for this app are that" +
                            " the user be logged in before proceeding with an auction" +
                            " snipe, and that the auction have more than 150 seconds " +
                            "left until it ends. This application is currently only " +
                            "available for Microsoft Windows with Java SDK 1.8.");
        });
        menuHelp.getItems().add(about);
        menuBar.getMenus().addAll(menuFile, menuHelp);

        top.setPadding(new Insets(10));
        top.getChildren().add(menuBar);
        top.getChildren().add(new ImageView("eBaySnipe_half.jpg"));
        File file = new File("Cookie.data");
        String outputMessage;
        if(!file.exists())
        {
            outputMessage = "\nYou are not signed in! Please use the login option in the FILE menu\nto " +
                    "continue. After that, you may enter the information below.";
        }
        else
        {
            outputMessage = "\nPlease enter the eBay auction URL and snipe amount below.";
        }
            top.getChildren().add(new Label(outputMessage));
        pane.setTop(top);

        // Create the labels for the auction URL and snipe amount inputs.
        VBox left = new VBox(10);
        left.setAlignment(Pos.CENTER_RIGHT);
        left.setPadding(new Insets(10, 0, 10, 10));
        left.getChildren().add(new Label("eBay auction URL: "));
        left.getChildren().add(new Label("Snipe amount: "));
        pane.setLeft(left);

        // Create the text fields for the auction URL and snipe amount inputs.
        VBox center = new VBox(10);
        center.setAlignment(Pos.CENTER_LEFT);
        TextField url = new TextField();
        TextField price = new TextField();
        price.setPrefColumnCount(5);
        center.setPadding(new Insets(10, 10, 10, 0));
        center.getChildren().addAll(url, price);
        pane.setCenter(center);

        // Create the button and event handler to execute the auction snipe.
        VBox bottom = new VBox(10);
        bottom.setAlignment(Pos.CENTER);
        Button button = new Button("SNIPE AUCTION");
        button.setOnAction(event ->
        {
            try
            {
                Double.parseDouble(price.getText().trim());
                timerSchedule.snipe(price.getText().trim(), url.getText().trim());
            }catch (Exception e)
            {
                showInfoDialogue("Input Error","You need to enter a number (ex: 19.99) and a valid URL.");
            }
        });
        bottom.setPadding(new Insets(10));
        bottom.getChildren().add(button);
        pane.setBottom(bottom);

        // Set the stage and show the scene.
        stage.setScene(new Scene(pane));
        stage.setTitle("eBay Auction Sniper");
        stage.show();
    }

    /**
     * Method to show an info dialogue
     * @param title is the title of the dialogue
     * @param text is the text to be entered in the dialogue
     */
    private void showInfoDialogue (String title, String text)
    {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(text);

        alert.showAndWait();
    }
}
