import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/************************************************************************************************
 * Class to create a Bot object that has the toolset to aid in the sniping of an ebay auction
 *
 * @author John Reagan
 * @author Matthew Rokusek
 * @author Trenton Chramosta
 * @since 2022-01-23
 ***********************************************************************************************/
public class Bot
{
    private WebDriver driver;
    private String url;
    private String bidAmount;

    /**
     * Constructor to create a bot given the username, password, and web-driver
     *
     * @param driver is the driver to be used (will determine the browser)
     * @param bidAmount is the amount that the bid will be
     */
    Bot(WebDriver driver, String bidAmount, String url)
    {
        this.bidAmount = bidAmount;
        this.driver = driver;
        this.url = url;
    }

    /**
     * Constructor with just the driver
     *
     * @param inDriver is the driver to be used (will determine the browser)
     */
    Bot(WebDriver inDriver)
    {
        this(inDriver, "0", ""); // Give a default amount of 0
    }

    /**
     * Constructor with the driver and the url of the auction
     *
     * @param inDriver is the driver to be used (will determine the browser)
     */
    Bot(WebDriver inDriver, String url)
    {
        this(inDriver, "0", url); // Give a default bid amount of 0
    }

    /**
     * Method to place a bid on an item (it should take about 30 - 120 seconds)
     */
    protected void placeBid()
    {
        openPage(getUrl());

        // Enter the amount
        WebElement currentElement = getDriver().findElement(By.cssSelector("#MaxBidId"));
        currentElement.sendKeys(bidAmount);

        // Click on the bid button
        currentElement = getDriver().findElement(By.cssSelector("#bidBtn_btn"));
        currentElement.click();

        // Click on the confirm button
        currentElement = getSlowElement("#confirm_button");

        // Wait until there are Five seconds left
        String timeLeft;
        do
        {
            timeLeft = getSlowElement("#_counter_itemEndDate_timeLeft").getText();
        } while (!timeLeft.equals("5s"));

        // Bid on the item
        currentElement.click();

        // Close the driver
        getDriver().quit();
    }

    /**
     * Method to return the number of milliseconds until the end of the auction
     * @return long of the number of milliseconds until the end of the auction
     */
    protected long timeTillSnipe()
    {
        long millisecondsLeft = 0;

        // Get the string of the time left
        openPage(getUrl());
        String stringTimeLeft = getSlowElement("#vi-cdown_timeLeft").getText();

        // Add up the numbers from each category
        for (String timeAmount : stringTimeLeft.trim().split(" "))
        {
            long currentNumber = Long.parseLong(timeAmount.substring(0, timeAmount.length() - 1));

            switch (timeAmount.charAt(timeAmount.length()-1))
            {
                case 'd':
                    millisecondsLeft += currentNumber * 86400000;
                    break;
                case 'h':
                    millisecondsLeft += currentNumber * 3600000;
                    break;
                case 'm':
                    millisecondsLeft += currentNumber * 60000;
                    break;
                case 's':
                    millisecondsLeft += currentNumber * 1000;
                    break;
            }
        }

        return millisecondsLeft-150000;
    }

    /**
     * Method to get an element that does not load in right away (will be the case for separate windows)
     * @param cssSelector is the css selector text of the element
     * @return the element that is taking a while to load in
     */
    private WebElement getSlowElement(String cssSelector)
    {
        long startTime = System.currentTimeMillis(); // Capture when the search begins
        WebElement currentElement = null; // will never be passed through, but it makes the compiler happy
        boolean stillWaiting = true;
        do
        {
            // If the search takes to long, then the users did not use the input wrong, and
            // they have already opened their window
            if (System.currentTimeMillis() - startTime > 30000)
            {
                System.exit(0);
            }
            try
            {
                currentElement = getDriver().findElement(By.cssSelector(cssSelector));
                stillWaiting = false;
            }catch (Exception e)
            {
                // the object is not ready
            }
        }while (stillWaiting); // loop until the element is present

        return currentElement;
    }

    /**
     * Getter for the driver that is being used
     * @return the driver that is being used for the browser
     */
    private WebDriver getDriver()
    {
        return driver;
    }

    /**
     * Method to open a page given a url
     * @param url is the url of the page to open
     */
    private void openPage(String url)
    {
        getDriver().get(url);
    }

    /**
     * Setter for the url
     * @param url is the string of the url to be used
     */
    protected void setUrl(String url)
    {
        this.url = url;
    }

    /**
     * Setter for the bid amount
     * @param bidAmount is the amount of money that will be used to bid on
     */
    protected void setBidAmount(String bidAmount)
    {
        this.bidAmount = bidAmount;
    }

    /**
     * Getter for the URL
     * @return a string that is the url
     */
    private String getUrl()
    {
        return url;
    }

    /**
     * Setter for the driver
     * @param driver is the driver that is used to navigate on a browser
     */
    protected void setDriver(WebDriver driver)
    {
        this.driver = driver;
    }

    /**
     * Method to close the window of the driver that the bot has stored
     */
    protected void closeWindow()
    {
        driver.close();
    }
}
