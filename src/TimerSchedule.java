import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.*;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

/************************************************************************************************
 * Class to create a TimeSchedule object which can handle the creation and management of a bot
 * that is capable of sniping ebay auctions
 *
 * @author John Reagan
 * @author Matthew Rokusek
 * @author Trenton Chramosta
 * @since 2022-01-23
 ***********************************************************************************************/
public class TimerSchedule extends TimerTask
{
    private final static String COOKIES_FILE_NAME = "Cookie.data";
    private Bot bot;

    /**
     * Constructor to create TimerSchedule object
     */
    public TimerSchedule() {}

    /**
     * Method that initiates the sniping process
     * @param bidAmount is the amount of money that the snipe will be for
     * @param auctionUrl is the url of the auction that will be sniped
     */
    protected void snipe(String bidAmount, String auctionUrl)
    {

        bot = new Bot(loadDriver(), bidAmount, auctionUrl);

        Timer timer= new Timer();
        long delay= bot.timeTillSnipe();
        timer.schedule(this, delay);
        bot.closeWindow();
    }

    /**
     * Method to load the driver
     * @return is the fresh driver that has been loaded
     */
    private WebDriver loadDriver()
    {
        System.setProperty("webdriver.gecko.driver", "geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        try
        {
            loadCookies(driver);
        }catch (Exception e)
        {
            System.out.println("The driver could not be found.");
        }

        return driver;
    }

    /**
     * Called to prompt a sign in from the user (captures the cookies)
     */
    void login()
    {
        File file = new File("Cookie.data");
        try
        {
            file.delete();
        } catch (Exception e)
        {
            // if there is not a file (there very well might not be)
        }
        WebDriver driver = loadDriver();
        driver.get("https://signin.ebay.com/ws/eBayISAPI.dll?SignIn&ru=");
        while(!driver.getCurrentUrl().equals("https://www.ebay.com/"))
        {
            //waits
        }
        saveCookies(driver);
    }

    /**
     * Method to load the cookies from a file
     * @param driver is the driver that the cookies will be loaded into
     */
    private static void loadCookies(WebDriver driver)
    {
        // Try to open a cookie file and get a set of cookies
        try
        {
            driver.get("https://www.ebay.com/");
            FileInputStream fin = new FileInputStream(COOKIES_FILE_NAME);
            ObjectInputStream ois = new ObjectInputStream(fin);
            Set<Cookie> cookies =(Set<Cookie>) ois.readObject();
            ois.close();
            addCookie(driver, cookies);
            driver.get("https://www.ebay.com/");
        }catch (Exception e)
        {
            System.out.println("No cookie file could be found. Try signing in again.");
        }
    }

    /**
     * Method to add a set of cookies to a driver through elimination
     * @param driver is the driver that the cookies will be added to
     * @param cookies is the set of cookies to be added
     */
    private static void addCookie(WebDriver driver, Set<Cookie> cookies)
    {
        // Add all the cookies by trying them and recursively calling until the end
        for (Cookie cookie : cookies)
        {
            try
            {
                driver.manage().addCookie(cookie);
            } catch (Exception e) {
                cookies.remove(cookie);
                addCookie(driver, cookies);
            }
        }

    }

    /**
     * Method to save cookies from a browser (prompts a sign in)
     * @param driver the driver that the cookies will be saved from
     */
    private static void saveCookies(WebDriver driver)
    {
        try
        {
            driver.get("https://www.ebay.com/");
            Set<Cookie> cookies = driver.manage().getCookies();
            FileOutputStream fos = new FileOutputStream(COOKIES_FILE_NAME);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(cookies);
            oos.close();
        }catch (Exception e)
        {
            System.out.println("Error.");
        }
        driver.quit();

    }

    /**
     * Method to start the bot's placing of the bid after the wait
     */
    @Override
    public void run()
    {
        bot.setDriver(loadDriver());
        bot.placeBid();
        System.exit(0); // exit because the program has completed
    }
}
